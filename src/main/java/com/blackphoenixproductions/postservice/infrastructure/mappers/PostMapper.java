package com.blackphoenixproductions.postservice.infrastructure.mappers;

import com.blackphoenixproductions.postservice.domain.model.Post;
import com.blackphoenixproductions.postservice.infrastructure.api.dto.EditPostDTO;
import com.blackphoenixproductions.postservice.infrastructure.api.dto.InsertPostDTO;
import com.blackphoenixproductions.postservice.infrastructure.entity.MVPostEntity;
import com.blackphoenixproductions.postservice.infrastructure.entity.PostEntity;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface PostMapper {
    @Mapping(target = "user.id", source = "userId")
    @Mapping(target = "topic.id", source = "topicId")
    Post insertPostDTOtoPost(InsertPostDTO insertPostDTO);
    Post editPostDTOtoPost(EditPostDTO editPostDTO);
    @Mapping(target = "topic.id", source = "topicId")
    @Mapping(target = "topic.title", source = "topicTitle")
    @Mapping(target = "user.id", source = "userId")
    @Mapping(target = "user.username", source = "postAuthorUsername")
    @Mapping(target = "user.role", source = "postAuthorRole")
    @Mapping(target = "user.email", source = "postAuthorEmail")
    @Mapping(target = "topic.user.id", source = "topicAuthorId")
    @Mapping(target = "topic.user.username", source = "topicAuthorUsername")
    @Mapping(target = "topic.user.role", source = "topicAuthorRole")
    @Mapping(target = "topic.user.email", source = "topicAuthorEmail")
    Post mvPostEntitytoPost(MVPostEntity mvPostEntity);
    @Mapping(target = "user.id", source = "userId")
    @Mapping(target = "topic.id", source = "topicId")
    Post postEntitytoPost(PostEntity PostEntity);
    @Mapping(target = "userId", source = "post.user.id")
    @Mapping(target = "topicId", source = "post.topic.id")
    PostEntity postToPostEntity(Post post);

}
