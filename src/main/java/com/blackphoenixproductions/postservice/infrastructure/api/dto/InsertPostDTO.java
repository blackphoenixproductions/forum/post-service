package com.blackphoenixproductions.postservice.infrastructure.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Schema(description = "Il post da creare.")
public class InsertPostDTO {
    @NotEmpty(message = "Il messaggio del post non puo' essere null/vuoto")
    @Size(max = 20000, message = "Il messaggio del post ha superato il massimo di 20000 caratteri")
    @Schema(description = "Il messaggio del post.")
    private String message;
    @NotNull(message = "L'id del topic non puo' essere null")
    @Schema(description = "L'id del del topic a cui appartiene il post.")
    private Long topicId;
    @NotNull(message = "L'id dell'utente non puo' essere null")
    @Schema(description = "L'id dell'utente a cui appartiene il post.")
    private Long userId;
}
