package com.blackphoenixproductions.postservice.infrastructure.api;

import com.blackphoenixproductions.postservice.domain.IPostDAO;
import com.blackphoenixproductions.postservice.domain.IPostService;
import com.blackphoenixproductions.postservice.domain.model.Post;
import com.blackphoenixproductions.postservice.infrastructure.api.dto.EditPostDTO;
import com.blackphoenixproductions.postservice.infrastructure.api.dto.InsertPostDTO;
import com.blackphoenixproductions.postservice.infrastructure.mappers.PostMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/posts")
@Tag(name = "Post", description = "endpoints riguardanti i post.")
public class PostRestAPIController {

    private static final Logger logger = LoggerFactory.getLogger(PostRestAPIController.class);

    private final IPostDAO postDAO;
    private final IPostService postService;
    private final PostMapper postMapper;

    @Autowired
    public PostRestAPIController(IPostDAO postDAO, IPostService postService, PostMapper postMapper) {
        this.postDAO = postDAO;
        this.postService = postService;
        this.postMapper = postMapper;
    }


    @Operation(summary = "Restituisce il numero totale dei post.")
    @GetMapping(value = "/public/getTotalPosts")
    public ResponseEntity<Long> getTotalPosts (HttpServletRequest req){
        Long totalPosts = postDAO.count();
        return new ResponseEntity<Long>(totalPosts, HttpStatus.OK);
    }

    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "400", description = "Bad request.", content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = "404", description = "Not Found.", content = @Content(schema = @Schema(hidden = true))),
    })
    @Operation(summary = "Cerca tutti i post contenuti in una pagina di un topic.")
    @PostMapping(value = "/public/findPostsByPage")
    public ResponseEntity<Page<Post>> findPostsByPage (@Parameter(description = "L'id del topic.") @RequestParam Long topicId,
                                                 @ParameterObject @PageableDefault(sort = {"createDate"}, direction = Sort.Direction.ASC) Pageable pageable){
        Page<Post> pagedPosts = postDAO.getPagedPosts(topicId, pageable);
        return new ResponseEntity<Page<Post>>(pagedPosts, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('USER', 'STAFF', 'HELPDESK')")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "400", description = "Bad request.", content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = "404", description = "Not Found.", content = @Content(schema = @Schema(hidden = true))),
    })
    @Operation(summary = "Crea un nuovo post.")
    @PostMapping(value = "createPost")
    public ResponseEntity<Post> createPost(@RequestBody @Valid InsertPostDTO postDTO, HttpServletRequest req){
        logger.info("Start createPost");
        Post savedPost = postService.createPost(postMapper.insertPostDTOtoPost(postDTO));
        logger.info("End createPost");
        return new ResponseEntity<Post>(savedPost, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('USER', 'STAFF', 'HELPDESK')")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "400", description = "Bad request", content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = "404", description = "Not Found.", content = @Content(schema = @Schema(hidden = true))),
    })
    @Operation(summary = "Modifica un post.")
    @PostMapping(value = "editPost")
    public ResponseEntity<Post> editPost(@RequestBody @Valid EditPostDTO postDTO, HttpServletRequest req){
        logger.info("Start editPost - post id : {}", postDTO.getId());
        Post editedPost = postService.editPost(postMapper.editPostDTOtoPost(postDTO));
        logger.info("End editPost");
        return new ResponseEntity<Post>(editedPost, HttpStatus.OK);
    }

}


