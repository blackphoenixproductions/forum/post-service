package com.blackphoenixproductions.postservice.infrastructure.repository;


import com.blackphoenixproductions.postservice.infrastructure.entity.MVPostEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface MVPostEntityRepository extends JpaRepository<MVPostEntity, Long>, JpaSpecificationExecutor<MVPostEntity> {
    @Modifying
    @Query(value = "REFRESH MATERIALIZED VIEW CONCURRENTLY mv_posts with data", nativeQuery=true)
    void refreshMVPosts();
    Page<MVPostEntity> findByTopicIdAndDeleteDateIsNull(Long topicId, Pageable pageable);

}
