package com.blackphoenixproductions.postservice.infrastructure.repository;

import com.blackphoenixproductions.postservice.infrastructure.entity.PostEntity;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PostEntityRepository extends JpaRepository<PostEntity, Long> {
}
