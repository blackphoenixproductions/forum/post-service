package com.blackphoenixproductions.postservice.infrastructure.dao;

import com.blackphoenixproductions.commons.exception.CustomException;
import com.blackphoenixproductions.postservice.domain.model.Post;
import com.blackphoenixproductions.postservice.domain.IPostDAO;
import com.blackphoenixproductions.postservice.infrastructure.entity.MVPostEntity;
import com.blackphoenixproductions.postservice.infrastructure.entity.PostEntity;
import com.blackphoenixproductions.postservice.infrastructure.mappers.PostMapper;
import com.blackphoenixproductions.postservice.infrastructure.repository.MVPostEntityRepository;
import com.blackphoenixproductions.postservice.infrastructure.repository.PostEntityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public class PostDAO implements IPostDAO {

    private static final Logger logger = LoggerFactory.getLogger(PostDAO.class);
    private final MVPostEntityRepository mvPostEntityRepository;
    private final PostEntityRepository postEntityRepository;
    private final PostMapper postMapper;

    @Autowired
    public PostDAO(MVPostEntityRepository mvPostEntityRepository, PostEntityRepository postEntityRepository, PostMapper postMapper) {
        this.mvPostEntityRepository = mvPostEntityRepository;
        this.postEntityRepository = postEntityRepository;
        this.postMapper = postMapper;
    }

    @Transactional(readOnly = true)
    @Override
    public Long count() {
        return postEntityRepository.count();
    }

    @Transactional
    @Override
    public Post createPost(Post post) {
        PostEntity postEntity = postMapper.postToPostEntity(post);
        PostEntity savedPost = postEntityRepository.saveAndFlush(postEntity);
        MVPostEntity mvPostEntity = mvPostEntityRepository.findById(savedPost.getId()).get();
        return postMapper.mvPostEntitytoPost(mvPostEntity);
    }

    @Transactional
    @Override
    public Post editPost(Post post) {
        Optional<PostEntity> findedPost = postEntityRepository.findById(post.getId());
        if(!findedPost.isPresent()){
            throw new CustomException("Post not found", HttpStatus.NOT_FOUND);
        }
        PostEntity postEntity = findedPost.get();
        postEntity.setMessage(post.getMessage());
        postEntity.setEditDate(post.getEditDate());
        PostEntity savedPost = postEntityRepository.saveAndFlush(postEntity);
        MVPostEntity mvPostEntity = mvPostEntityRepository.findById(savedPost.getId()).get();
        return postMapper.mvPostEntitytoPost(mvPostEntity);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<Post> getPagedPosts(Long topicId, Pageable pageable) {
        Page<MVPostEntity> pageMvPostEntity = mvPostEntityRepository.findByTopicIdAndDeleteDateIsNull(topicId, pageable);
        Page<Post> postPage = pageMvPostEntity.map(mvPostEntity -> postMapper.mvPostEntitytoPost(mvPostEntity));
        return postPage;
    }

    @Transactional(readOnly = true)
    @Override
    public Post getPost(Long id) {
        return postMapper.mvPostEntitytoPost(mvPostEntityRepository.findById(id).get());
    }

    @Transactional
    @Override
    public void refreshMaterializedView() {
        logger.info("Refreshing MV_POSTS...");
        mvPostEntityRepository.refreshMVPosts();
        logger.info("MV_POSTS refreshed");
    }
}
