package com.blackphoenixproductions.postservice.infrastructure.messagebroker;

import com.blackphoenixproductions.commons.constants.Groups;
import com.blackphoenixproductions.commons.constants.Topics;
import com.blackphoenixproductions.postservice.domain.IPostDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaConsumer {

    private static final Logger logger = LoggerFactory.getLogger(KafkaConsumer.class);

    private final IPostDAO postDAO;

    @Autowired
    public KafkaConsumer(IPostDAO postDAO) {
        this.postDAO = postDAO;
    }


    @KafkaListener(groupId = Groups.POSTS_CONSUMER, topics = Topics.REFRESH_MV_POSTS)
    public void listen(String message) {
        logger.info(message);
        postDAO.refreshMaterializedView();
    }

}
