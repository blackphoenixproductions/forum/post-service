package com.blackphoenixproductions.postservice.infrastructure.messagebroker;

import com.blackphoenixproductions.commons.constants.Topics;
import com.blackphoenixproductions.postservice.domain.MessagePublisher;
import com.blackphoenixproductions.postservice.domain.model.Post;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class KafkaProducer implements MessagePublisher {

    private static final Logger logger = LoggerFactory.getLogger(KafkaProducer.class);

    private final KafkaTemplate<String, String> kafkaTemplate;
    private final ObjectMapper objectMapper;

    @Autowired
    public KafkaProducer(KafkaTemplate<String, String> kafkaTemplate, ObjectMapper objectMapper) {
        this.kafkaTemplate = kafkaTemplate;
        this.objectMapper = objectMapper;
    }

    public void refreshMvTopics()
    {
        logger.info("Sending message to topic-service...");
        this.kafkaTemplate.send(Topics.REFRESH_MV_TOPICS, "refresh");
    }

    public void sendNotification(Post post) {
        logger.info("Sending message to notification-service...");
        try {
            this.kafkaTemplate.send(Topics.NOTIFICATIONS, objectMapper.writeValueAsString(post));
        } catch (JsonProcessingException ex){
            logger.error("Conversion error : ", ex);
        }
    }


}
