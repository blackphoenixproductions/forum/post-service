package com.blackphoenixproductions.postservice.infrastructure.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Immutable;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Immutable
@Entity
@Table(name = "MV_POSTS")
public class MVPostEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String message;
    @Column
    private LocalDateTime createDate;
    @Column
    private LocalDateTime deleteDate;
    @Column
    private LocalDateTime editDate;
    @Column
    private Long topicId;
    @Column
    private Long userId;
    @Column
    private String postAuthorUsername;
    @Column
    private String postAuthorRole;
    @Column
    private String postAuthorEmail;
    @Column
    private String topicTitle;
    @Column
    private boolean topicEmailUser;
    @Column
    private Long topicAuthorId;
    @Column
    private String topicAuthorUsername;
    @Column
    private String topicAuthorRole;
    @Column
    private String topicAuthorEmail;

}
