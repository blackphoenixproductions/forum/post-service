package com.blackphoenixproductions.postservice.infrastructure.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Table(name = "POSTS")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PostEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String message;
    @Column
    private LocalDateTime createDate;
    @Column
    private LocalDateTime deleteDate;
    @Column
    private LocalDateTime editDate;
    @Column
    private Long topicId;
    @Column
    private Long userId;

}
