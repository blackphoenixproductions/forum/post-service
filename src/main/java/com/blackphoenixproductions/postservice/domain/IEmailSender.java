package com.blackphoenixproductions.postservice.domain;

import com.blackphoenixproductions.postservice.domain.model.Post;
import com.blackphoenixproductions.postservice.domain.model.User;

public interface IEmailSender {
    void sendTopicReplyEmail(User user, User userReply, Post post);
}
