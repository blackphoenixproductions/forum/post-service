package com.blackphoenixproductions.postservice.domain;

import com.blackphoenixproductions.postservice.domain.model.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IPostDAO {
    Long count();
    Post createPost(Post post);
    Post editPost(Post post);
    Page<Post> getPagedPosts(Long topicId, Pageable pageable);
    Post getPost(Long id);
    void refreshMaterializedView();
}
