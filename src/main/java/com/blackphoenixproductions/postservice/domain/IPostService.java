package com.blackphoenixproductions.postservice.domain;

import com.blackphoenixproductions.postservice.domain.model.Post;

import java.util.Set;

public interface IPostService {
    Post createPost(Post post);
    Post editPost(Post post);
}
