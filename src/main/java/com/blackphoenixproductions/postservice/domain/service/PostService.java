package com.blackphoenixproductions.postservice.domain.service;

import com.blackphoenixproductions.commons.enums.Pagination;
import com.blackphoenixproductions.postservice.domain.IEmailSender;
import com.blackphoenixproductions.postservice.domain.IPostDAO;
import com.blackphoenixproductions.postservice.domain.IPostService;
import com.blackphoenixproductions.postservice.domain.MessagePublisher;
import com.blackphoenixproductions.postservice.domain.model.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;


@Service
public class PostService implements IPostService {

    private final IEmailSender emailSender;
    private final IPostDAO postDAO;
    private final MessagePublisher messagePublisher;

    @Autowired
    public PostService(IEmailSender emailSender, IPostDAO postDAO, MessagePublisher messagePublisher) {
        this.emailSender = emailSender;
        this.postDAO = postDAO;
        this.messagePublisher = messagePublisher;
    }

    @Override
    public Post createPost(Post post) {
        post.setCreateDate(LocalDateTime.now());
        Post savedPost = postDAO.createPost(post);
        if (userIsNotTopicAuthorPost(savedPost) && savedPost.getTopic().isEmailUser()) {
            emailSender.sendTopicReplyEmail(savedPost.getTopic().getUser(), savedPost.getUser(), savedPost);
        }
        messagePublisher.refreshMvTopics();
        if(userIsNotTopicAuthor(savedPost)) {
            int lastPageNumber = getLastPageNumber(savedPost);
            savedPost.setLastPageNumber(lastPageNumber);
            messagePublisher.sendNotification(savedPost);
        }
        return savedPost;
    }

    private int getLastPageNumber(Post savedPost) {
        return postDAO.getPagedPosts(savedPost.getTopic().getId(),
                PageRequest.of(0, Math.toIntExact(Pagination.POST_PAGINATION.getValue()), Sort.by("createDate").ascending()))
                .getTotalPages()-1;
    }

    @Override
    public Post editPost(Post post) {
        post.setEditDate(LocalDateTime.now());
        Post savedPost = postDAO.editPost(post);
        messagePublisher.refreshMvTopics();
        return savedPost;
    }

    private static boolean userIsNotTopicAuthorPost(Post savedPost) {
        return !savedPost.getUser().equals(savedPost.getTopic().getUser());
    }

    private static boolean userIsNotTopicAuthor(Post post) {
        return !post.getUser().equals(post.getTopic().getUser());
    }
}
