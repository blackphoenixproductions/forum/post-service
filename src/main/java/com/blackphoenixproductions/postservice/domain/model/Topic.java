package com.blackphoenixproductions.postservice.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Topic {
    private Long id;
    private String title;
    private User user;
    private boolean emailUser;
}
