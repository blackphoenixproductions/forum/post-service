package com.blackphoenixproductions.postservice.domain;

import com.blackphoenixproductions.postservice.domain.model.Post;

public interface MessagePublisher {
    void refreshMvTopics();
    void sendNotification(Post post);
}
