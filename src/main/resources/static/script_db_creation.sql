create database post;

CREATE TABLE POSTS (
   id bigserial primary key not null,
   message text,
   create_date timestamp,
   delete_date timestamp,
   edit_date timestamp,
   user_id bigint not null,
   topic_id bigint not null
);

CREATE EXTENSION postgres_fdw;

CREATE SERVER postgres_fdw_topic FOREIGN DATA WRAPPER postgres_fdw
OPTIONS (host 'localhost', dbname 'topic');
CREATE USER MAPPING FOR PUBLIC SERVER postgres_fdw_topic
OPTIONS (password '');
CREATE FOREIGN TABLE topics (id bigserial, title varchar(255), pinned boolean, email_user boolean, create_date timestamp, delete_date timestamp, edit_date timestamp, message text, user_id bigint not null)
SERVER postgres_fdw_topic
OPTIONS (table_name 'topics');

CREATE SERVER postgres_fdw_user FOREIGN DATA WRAPPER postgres_fdw
OPTIONS (host 'localhost', dbname 'user');
CREATE USER MAPPING FOR PUBLIC SERVER postgres_fdw_user
OPTIONS (password '');
CREATE FOREIGN TABLE users (id bigserial, username varchar(255), email varchar(255), role varchar(255))
SERVER postgres_fdw_user
OPTIONS (table_name 'users');


CREATE MATERIALIZED VIEW MV_POSTS AS
select p.*, u1.username as post_author_username, u1.role as post_author_role, u1.email as post_author_email,
       t.title as topic_title, t.email_user as topic_email_user, u2.id as topic_author_id, u2.username as topic_author_username,  u2.role as topic_author_role, u2.email as topic_author_email
from POSTS p, TOPICS t, USERS u1, USERS u2
where
        p.topic_id = t.id and
        p.user_id = u1.id and
        t.user_id = u2.id;

CREATE UNIQUE INDEX ON MV_POSTS(id);

CREATE OR REPLACE FUNCTION refresh_mv_posts()
   RETURNS trigger
   LANGUAGE plpgsql
AS
$$
BEGIN
	REFRESH MATERIALIZED VIEW CONCURRENTLY mv_posts with data;
    RETURN NULL;
END;
$$;

CREATE TRIGGER trigger_refresh_mv_posts AFTER UPDATE OR INSERT OR DELETE ON posts
FOR EACH STATEMENT
EXECUTE PROCEDURE refresh_mv_posts();